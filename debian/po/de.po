#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: exult\n"
"Report-Msgid-Bugs-To: jorda@ettin.org\n"
"POT-Creation-Date: 2006-08-03 21:47+0200\n"
"PO-Revision-Date: 2004-08-07 14:10+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Path to \"The Black Gate\":"
msgstr "Pfad zu �The Black Gate�:"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Exult needs to know where your copy of Ultima VII: The Black Gate is "
"located. You need to give the path where the game's top directory can be "
"found."
msgstr ""
"Exult muss wissen, wo sich Ihre Kopie von Ultima VII: The Black Gate "
"befindet. Sie m�ssen den Pfad zu dem obersten Verzeichnis des Spiels "
"eingeben."

#. Type: string
#. Description
#: ../templates:1001
msgid "If you don't have BG, just leave the field blank."
msgstr "Falls Sie BG nicht besitzen, lassen Sie das Feld leer."

#. Type: string
#. Description
#: ../templates:2001
msgid "Path to \"Serpent Isle\":"
msgstr "Pfad zu �Sperpent Isle�:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"Exult needs to know where your copy of Ultima VII: Serpent Isle is located. "
"You need to give the path where the game's top directory can be found."
msgstr ""
"Exult muss wissen, wo sich Ihre Kopie von Ultima VII: Serpent Isle "
"befindet. Sie m�ssen den Pfad zu dem obersten Verzeichnis des Spiels "
"eingeben."

#. Type: string
#. Description
#: ../templates:2001
msgid "If you don't have SI, just leave the field blank."
msgstr "Falls Sie SI nicht besitzen, lassen Sie das Feld leer."

#. Type: note
#. Description
#: ../templates:3001
msgid "The entered path is not a directory"
msgstr "Der angegebene Pfad ist kein Verzeichnis"

#. Type: note
#. Description
#: ../templates:3001
msgid ""
"Please enter the path to the top directory of the game, or leave it blank "
"if you don't have it."
msgstr ""
"Bitte geben Sie den Pfad zum obersten Verzeichnis des Spieles an, oder "
"lassen Sie es leer, falls Sie es nicht haben."

#. Type: note
#. Description
#: ../templates:4001
msgid "Invalid Ultima top directory"
msgstr "Ung�ltiges oberstes Verzeichnis von Ultima"

#. Type: note
#. Description
#: ../templates:4001
msgid ""
"The entered path does not look like a Ultima VII top directory. "
"(Specifically, a subdirectory named \"static\" was expected but did not "
"exist.)"
msgstr ""
"Der eingegebene Pfad sieht nicht nach einem obersten Verzeichnis von Ultima "
"VII aus (insbesondere wurde ein Unterverzeichnis mit Name �static� "
"erwartet, das aber nicht existiert)."

#~ msgid "This does not look like a top directory of one of the Ultimas."
#~ msgstr ""
#~ "Das Verzeichnis sieht nicht wie das oberste Verzeichnis eines Ultima-"
#~ "Spieles aus."

#~ msgid ""
#~ "(Specifically, I was looking for a subdirectory named \"static\", which "
#~ "did not exist.)"
#~ msgstr ""
#~ "(Insbesondere wurde nach einem Unterverzeichnis namens �static� gesucht, "
#~ "welches nicht existiert.)"

#~ msgid "Where is The Black Gate installed?"
#~ msgstr "Wo ist The Black Gate installiert?"

#~ msgid "Where is Serpent Isle installed?"
#~ msgstr "Wo ist Serpent Isle installiert?"
